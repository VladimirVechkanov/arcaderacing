﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars {
	public abstract class BaseInputComponent : MonoBehaviour {
		public float Acceleration { get; protected set; }
		public float Rotate { get; protected set; }

		public event EventHandler<bool> OnHandBrake;

		protected abstract void FixedUpdate();
		protected virtual void Start() { }

		protected void CallHandBrake(bool value) => OnHandBrake?.Invoke(null, value);
	}
}