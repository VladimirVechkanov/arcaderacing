﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars {
	public class BotInputComponent : BaseInputComponent {
		[SerializeField]
		private List<Transform> _points;

		protected override void FixedUpdate() {
			if(TimerComponent.StartTimer > 0) {
				Rotate = 0f;
				Acceleration = 0f;
				return;
			}

			Rotate = BotAngle();
			Acceleration = 1f;
		}

		protected override void Start() {
			base.Start();

			foreach(var item in _points) {
				item.GetComponent<MeshRenderer>().enabled = false;
			}
		}

		private float BotAngle() {
			if(_points.Count == 0) return 0f;

			var targetPos = _points[0].transform.position;
			targetPos.y = transform.position.y;

			var direction = targetPos - transform.position;

			return -Vector3.SignedAngle(direction, transform.forward, transform.up);
		}

		private void OnTriggerEnter(Collider other) {
			if(other.tag == "BotPoint") {
				_points.Remove(other.transform);
				Destroy(other.gameObject);
			}
		}
	}
}