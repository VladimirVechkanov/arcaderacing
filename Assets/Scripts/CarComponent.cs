﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars {
	[RequireComponent(typeof(Rigidbody), typeof(BaseInputComponent), typeof(WheelsComponent))]
	public class CarComponent : MonoBehaviour {
		private BaseInputComponent _inputs;
		private WheelsComponent _wheels;
		private Rigidbody _rigidBody;

		[SerializeField, Range(1f, 40f)]
		private float _maxSteerAngle = 35f;
		[SerializeField]
		private float _torque = 2500f;
		[SerializeField, Range(0f, float.MaxValue)]
		private float _handBrakeForce = float.MaxValue;
		[SerializeField]
		private Vector3 _centerOfMass;

		private void Start() {
			_wheels = GetComponent<WheelsComponent>();
			_inputs = GetComponent<BaseInputComponent>();
			_rigidBody = GetComponent<Rigidbody>();

			_inputs.OnHandBrake += (q, qq) => OnHandBrake(qq);

			_rigidBody.centerOfMass = _centerOfMass;
		}

		private void FixedUpdate() {
			OnRotate();
			OnDrive();
		}

		private void OnHandBrake(bool value) {
			if(value) {
				foreach(var wheel in _wheels.GetRearColliders) {
					wheel.brakeTorque = _handBrakeForce;
				}
			}
			else {
				foreach(var wheel in _wheels.GetRearColliders) {
					wheel.brakeTorque = 0f;
				}
			}
		}

		private void OnRotate() {
			_wheels.UpdateVisual(_inputs.Rotate * _maxSteerAngle);
		}

		private void OnDrive() {
			var torque = _inputs.Acceleration * _torque / 2f;
			foreach(var wheel in _wheels.GetRearColliders) {
				wheel.motorTorque = torque;
			}
		}

		private void OnDrawGizmos() {
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(transform.TransformPoint(_centerOfMass), 0.5f);
		}
	}
}