﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars {
	public class FinishComponent : MonoBehaviour {
		[SerializeField]
		private Text _winText;

		public static bool Finish;

		private void Awake() {
			Finish = false;
		}

		private void OnTriggerEnter(Collider other) {
			var player = other.GetComponentInParent<PlayerInputComponent>();
			var bot = other.GetComponentInParent<BotInputComponent>();

			if(player != null) {
				_winText.gameObject.SetActive(true);
			}
			else if(bot != null) {
				_winText.text = $"{bot.name} Win!";
				_winText.gameObject.SetActive(true);
			}

			GetComponent<BoxCollider>().enabled = false;
			Finish = true;
		}
	}
}