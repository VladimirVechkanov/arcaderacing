﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars {
	public class PlayerInputComponent : BaseInputComponent {
		private CarControls _controls;

		[SerializeField]
		private Camera _frontCamera, _rearCamera;

		protected override void FixedUpdate() {
			if(TimerComponent.StartTimer > 0) {
				Rotate = 0f;
				Acceleration = 0f;
				return;
			}

			//Rotation
			var direction = _controls.Car.Rotate.ReadValue<float>();

			if(direction == 0f && Rotate != 0f) {
				Rotate = Rotate > 0f
					? Rotate - Time.fixedDeltaTime
					: Rotate + Time.fixedDeltaTime;
			}
			else {
				Rotate = Mathf.Clamp(Rotate + direction * Time.fixedDeltaTime, -1f, 1f);
			}

			//Drive
			Acceleration = _controls.Car.Acceleration.ReadValue<float>();
		}

		#region

		private void Awake() {
			_controls = new CarControls();
		}

		private void OnDestroy() {
			_controls.Dispose();
		}

		private void OnEnable() {
			_controls.Car.Enable();
		}

		private void OnDisable() {
			_controls.Car.Disable();
		}

		#endregion

		protected override void Start() {
			base.Start();

			_controls.Car.HandBrake.performed += (q) => CallHandBrake(true);
			_controls.Car.HandBrake.canceled += (q) => CallHandBrake(false);

			_controls.Car.RearViev.performed += (q) => OnRearViev(true);
			_controls.Car.RearViev.canceled += (q) => OnRearViev(false);
		}

		private void OnRearViev(bool value) {
			if(value) {
				_frontCamera.gameObject.SetActive(false);
				_rearCamera.gameObject.SetActive(true);
			}
			else {
				_frontCamera.gameObject.SetActive(true);
				_rearCamera.gameObject.SetActive(false);
			}
		}
	}
}