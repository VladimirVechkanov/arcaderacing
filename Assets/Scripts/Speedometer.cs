﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars {
	public class Speedometer : MonoBehaviour {
		private const float c_convertMeterInSecFromKmInH = 3.6f;
		private Transform _car;

		[SerializeField]
		private float _maxSpeed = 300f;
		[SerializeField]
		private Color _minColor;
		[SerializeField]
		private Color _maxColor;
		[SerializeField, Range(0.1f, 1f)]
		private float _delay = 0.3f;
		[SerializeField]
		private Text _text;

		private void Start() {
			_car = FindObjectOfType<PlayerInputComponent>().transform;
			StartCoroutine(Speed());
		}

		private IEnumerator Speed() {
			var prevPos = _car.position;
			while(true) {
				var distance = Vector3.Distance(_car.position, prevPos);
				var speed = (float)System.Math.Round(distance / _delay * c_convertMeterInSecFromKmInH, 1);

				_text.color = Color.Lerp(_minColor, _maxColor, speed / _maxSpeed);
				_text.text = speed.ToString();
				prevPos = _car.position;

				yield return new WaitForSeconds(_delay);
			}
		}
	}
}