﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars {
	public class StopWatchComponent : MonoBehaviour {
		private int _min;
		private int _sec;
		private int _dSec;
		private bool _start;

		[SerializeField]
		private Text _text;

		private void Update() {
			if(!FinishComponent.Finish) {
				var text = "";
				if(_min < 10) {
					text += "0" + _min.ToString() + ":";
				}
				else {
					text += _min.ToString() + ":";
				}
				if(_sec < 10) {
					text += "0" + _sec.ToString() + ":";
				}
				else {
					text += _sec.ToString() + ":";
				}

				text += _dSec.ToString();

				_text.text = "";
				_text.text = text;
			}

			if(TimerComponent.StartTimer == 0 && !_start) {
				StartCoroutine(Min());
				StartCoroutine(Sec());
				StartCoroutine(DSec());
				_start = true;
			}
		}

		private IEnumerator Min() {
			while(true) {
				yield return new WaitForSeconds(60f);

				_min++;
				if(_min == 100) {
					_min = 0;
				}
			}
		}
		private IEnumerator Sec() {
			while(true) {
				yield return new WaitForSeconds(1f);

				_sec++;
				if(_sec == 60) {
					_sec = 0;
				}
			}
		}
		private IEnumerator DSec() {
			while(true) {
				yield return new WaitForSeconds(0.1f);

				_dSec++;
				if(_dSec == 10) {
					_dSec = 0;
				}
			}
		}
	}
}