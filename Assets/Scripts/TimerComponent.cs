﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars {
	public class TimerComponent : MonoBehaviour {
		[SerializeField]
		private Text _text;

		public static int StartTimer;
		
		private Animation _animation;

		private void Start() {
			_animation = GetComponent<Animation>();
			StartCoroutine(Timer());
		}
		
		private IEnumerator Timer() {
			StartTimer = 4;
			while(StartTimer > 0) {
				StartTimer--;
				_text.text = StartTimer.ToString();
				_animation.Play();

				yield return new WaitForSeconds(StartTimer == 0 ? 0.1f : 1f);
			}
			_text.gameObject.SetActive(false);
		}
	}
}