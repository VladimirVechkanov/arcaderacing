﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars {
	public class WheelsComponent : MonoBehaviour {
		private Transform[] _frontMeshes, _rearMeshes;
		private WheelCollider[] _frontColliders, _rearColliders, _allColliders;

		[SerializeField]
		private Transform _leftFrontMesh, _rightFrontMesh, _leftRearMesh, _rightRearMesh;
		[SerializeField]
		private WheelCollider _leftFrontCollider, _rightFrontCollider, _leftRearCollider, _rightRearCollider;

		public WheelCollider[] GetFrontColliders => _frontColliders;
		public WheelCollider[] GetRearColliders => _rearColliders;
		public WheelCollider[] GetAllColliders => _allColliders;

		private void Start() {
			_frontMeshes = new Transform[] { _leftFrontMesh, _rightFrontMesh };
			_rearMeshes  = new Transform[] { _leftRearMesh, _rightRearMesh };

			_frontColliders = new WheelCollider[] { _leftFrontCollider, _rightFrontCollider };
			_rearColliders  = new WheelCollider[] { _leftRearCollider, _rightRearCollider };
			_allColliders   = new WheelCollider[] { _leftFrontCollider, _rightFrontCollider, _leftRearCollider, _rightRearCollider };
		}

		public void UpdateVisual(float angle) {
			for(int i = 0; i < _frontColliders.Length; i++) {
				_frontColliders[i].steerAngle = angle;
				_frontColliders[i].GetWorldPose(out var position, out var rotation);
				_frontMeshes[i].position = position;
				_frontMeshes[i].rotation = rotation;

				_rearColliders[i].GetWorldPose(out position, out rotation);
				_rearMeshes[i].position = position;
				_rearMeshes[i].rotation = rotation;
			}
		}
	}
}